use std::{
    fs::{File, OpenOptions},
    io::{stderr, stdin, stdout, BufReader, IsTerminal, Stdout, Write},
};

use clap::{Parser, Subcommand};
use color_eyre::eyre::bail;
use fritzdecode::{markers::Unverified, ConfigBackup};

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    operation: Operation,
}

#[derive(Subcommand)]
enum Operation {
    /// Decrypts data in a given config file.
    Decrypt {
        file: String,
        /// Ignore an invalid CRC32 checksum in the input
        #[arg(long)]
        ignore_invalid_crc: bool,
        /// File to write the decrypted config to. Uses standard output if not
        /// specified.
        #[arg(short, long)]
        output: Option<String>,
        /// If specified, extract only the given file record.
        #[arg(short = 'f', long)]
        extract_file: Option<String>,
    },
    /// Verifies the integrity (decryption key, CRC32) of the given config file.
    Verify { file: Option<String> },
    /// Generates the correct CRC32 checksum for a given config file.
    #[command(name = "gencrc")]
    GenCrc { file: Option<String> },
}

fn load_file(file: Option<&str>) -> color_eyre::Result<ConfigBackup<Unverified>> {
    match file {
        None | Some("-") => {
            let config = ConfigBackup::load(BufReader::new(stdin()))?;
            Ok(config)
        }
        Some(file) => {
            let config = ConfigBackup::load(BufReader::new(File::open(file)?))?;
            Ok(config)
        }
    }
}

fn main() -> color_eyre::Result<()> {
    color_eyre::install()?;

    let args = Args::parse();
    match args.operation {
        Operation::Decrypt {
            file,
            ignore_invalid_crc,
            output,
            extract_file,
        } => {
            let config = load_file(Some(&file))?;

            eprint!("Password: ");
            stderr().flush()?;
            let mut password = String::new();
            stdin().read_line(&mut password)?;
            let password = password.trim_end_matches('\n');

            let decrypted = match config.verify_crc() {
                Ok(config) => config.decrypt(password)?,
                Err((unverified, e)) => {
                    if ignore_invalid_crc {
                        unverified.decrypt(password)?
                    } else {
                        return Err(e.into());
                    }
                }
            };

            let decrypted = decrypted.update_crc();

            let mut output = {
                enum Output {
                    File(File),
                    Stdout(Stdout),
                }

                impl Write for Output {
                    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
                        match self {
                            Output::File(f) => f.write(buf),
                            Output::Stdout(s) => s.write(buf),
                        }
                    }

                    fn flush(&mut self) -> std::io::Result<()> {
                        match self {
                            Output::File(f) => f.flush(),
                            Output::Stdout(s) => s.flush(),
                        }
                    }
                }

                impl Output {
                    fn is_terminal(&self) -> bool {
                        match self {
                            Output::File(f) => f.is_terminal(),
                            Output::Stdout(s) => s.is_terminal(),
                        }
                    }
                }

                if let Some(output) = output {
                    Output::File(
                        OpenOptions::new()
                            .write(true)
                            .create(true)
                            .truncate(true)
                            .open(output)?,
                    )
                } else {
                    Output::Stdout(stdout())
                }
            };

            if let Some(extract_file) = extract_file {
                let Some(file) = decrypted.files().iter().find(|f| f.name == extract_file) else {
                    bail!("No file record named {extract_file}");
                };

                match &file.content {
                    fritzdecode::FileContent::Plain(s) => {
                        write!(output, "{s}")?;
                    }
                    fritzdecode::FileContent::Binary(b) => {
                        if let Ok(s) = std::str::from_utf8(b) {
                            write!(output, "{s}")?;
                        } else if output.is_terminal() {
                            bail!("Refusing to write binary data to terminal");
                        } else {
                            output.write_all(b)?;
                        }
                    }
                }
            } else {
                decrypted.serialize(output)?;
            }
        }
        Operation::Verify { file } => {
            let config = load_file(file.as_deref())?;
            config.verify_crc().map_err(|(_unverified, e)| e)?;
        }
        Operation::GenCrc { file } => {
            let config = load_file(file.as_deref())?.update_crc();
            println!("{:08X}", config.crc());
        }
    }

    Ok(())
}

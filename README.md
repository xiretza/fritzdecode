# Tools for working with AVM fritzbox config files

WIP.

Installation: `cargo +nightly install fritzdecode`

Basic usage:
1. Export the configuration in the FritzBox web interface (set a password!)
2. Run `fritzdecode decrypt foo_config.export`, providing the password when prompted
